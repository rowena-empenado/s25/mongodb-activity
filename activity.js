/* ----------------------- Activity - Session 25 ---------------------------------- */

/*
	2. Use the count operator to count the total number of fruits on sale.
	3. Use the count operator to count the total number of fruits with stock more than 20.
	4. Use the average operator to get the average price of fruits on sale per supplier.
	5. Use the max operator to get the highest price of a fruit per supplier.
	6. Use the min operator to get the lowest price of a fruit per supplier.
*/

db.fruits.insertMany([
	{
		"name": "Banana",
		"supplier": "Farmer Fruits Inc.",
		"stocks": 30,
		"price": 20,
		"onSale": true
	},
	{
		"name": "Mango",
		"supplier": "Mango Magic Inc.",
		"stocks": 50,
		"price": 70,
		"onSale": true
	},
	{
		"name": "Dragon Fruit",
		"supplier": "Farmer Fruits Inc.",
		"stocks": 10,
		"price": 60,
		"onSale": true
	},
	{
		"name": "Grapes",
		"supplier": "Fruity Co.",
		"stocks": 30,
		"price": 100,
		"onSale": true
	},
	{
		"name": "Apple",
		"supplier": "Apple Valley",
		"stocks": 0,
		"price": 20,
		"onSale": false
	},
	{
		"name": "Papaya",
		"supplier": "Fruity Co.",
		"stocks": 15,
		"price": 60,
		"onSale": true
	}
])


db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $count: "totalNumberOfFruitsOnSale"  }
])

db.fruits.aggregate([
	{ $match: { "stocks": {$gt: 20 }} },
	{ $count: "totalNumberOfFruitsWithMoreThan20Stocks"  }
])

db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier", averagePricePerFruit: {$avg: "$price"} } }
])

db.fruits.aggregate([
	{ $group: { "_id": "$supplier", highestPriceOfAFruit: {$max: "$price"} } }
])

db.fruits.aggregate([
	{ $group: { "_id": "$supplier", lowestPriceOfAFruit: {$min: "$price"} } }
])


